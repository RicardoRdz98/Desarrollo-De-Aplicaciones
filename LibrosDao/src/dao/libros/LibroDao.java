package dao.libros;

import app.Libro;
import java.util.ArrayList;

/**
 *
 * @author YarelyGms
 */
public interface LibroDao {
    public Libro add(Libro libro);
    
    public void delete(int id);
    
    public void upDate(Libro libro);
    
    public Libro getLibro(int id);
    
    public ArrayList<Libro> getLibros();
}
