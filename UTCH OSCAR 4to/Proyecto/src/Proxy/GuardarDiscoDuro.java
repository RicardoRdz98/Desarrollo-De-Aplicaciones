package Proxy;

import Json.Json;

public class GuardarDiscoDuro implements IGuardar {

    @Override
    public void save(String datosAGuardar) {
        Json json = new Json();
        String save = json.SaveJson(datosAGuardar);
        System.out.println(save);
        Internet(datosAGuardar);
    }

    public void Internet(String datosAGuardar) {

        Thread hilo = new Thread(new Runnable() {
            int x = 0;

            @Override
            public void run() {
                try {
                    while (x < 100) {
                        x++;
                        Thread.sleep(10);
                    }
                    if (ConnectionManager.hayConexion()) {
                        new ObjetoRemoto().save(datosAGuardar);
                    } else {
                        new GuardarDiscoDuro().save(datosAGuardar);
                    }
                } catch (InterruptedException e) {
                    System.out.println(e);
                }
            }
        });
        hilo.start();
    }
}