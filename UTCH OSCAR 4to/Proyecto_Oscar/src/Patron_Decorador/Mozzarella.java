package Patron_Decorador;

public class Mozzarella extends ToppingDecorator {

    public Mozzarella(Pizza temporalPizza) { //Se agrega el queso y sus descripcion con precios
        super(temporalPizza);
        System.out.println("Agregar Mozzarella");
    }

    @Override
    public String getDescription() {
        return temporalPizza.getDescription() + ", mozzarella";
    }

    @Override
    public double getPrice() {
        System.out.println("Precio de la mozzarella: " + .50);
        return temporalPizza.getPrice() + .50;
    }

}