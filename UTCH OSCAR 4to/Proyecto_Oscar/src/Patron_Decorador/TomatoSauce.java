package Patron_Decorador;

public class TomatoSauce extends ToppingDecorator { //Se agrega el tomate sus descripcin y precio

    public TomatoSauce(Pizza temporalPizza) {
        super(temporalPizza);
        System.out.println("Agregar salsa de tomate");
    }

    @Override
    public String getDescription() {
        return temporalPizza.getDescription() + ", salsa de tomate";
    }

    @Override
    public double getPrice() {
        System.out.println("Precio de la salsa de tomate: " + 2.50);
        return temporalPizza.getPrice() + 2.50;
    }

}