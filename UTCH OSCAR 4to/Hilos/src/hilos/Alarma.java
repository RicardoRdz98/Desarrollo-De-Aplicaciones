package app;

import java.util.Objects;

@SuppressWarnings("rawtypes")
public class Alarma {

    private String hora = "";
    private String minuto = "";
    private String pmam = "";

    public Alarma() {
        // TODO Auto-generated constructor stub
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getMinuto() {
        return minuto;
    }

    public void setMinuto(String minuto) {
        this.minuto = minuto;
    }

    public String getPmap() {
        return pmam;
    }

    public void setPmap(String pmam) {
        this.pmam = pmam;
    }
	
    public Alarma(String hora, String minuto, String pmam) {
        super();
        this.hora = hora;
        this.minuto = minuto;
        this.pmam = pmam;
    }

//    @Override
//    public String toString() {
//        // TODO Auto-generated method stub
//        return this.hora+ ":" + this.minuto+ " " + this.pmam;
//        //return super.toString();
//    }

    @Override
    public boolean equals(Object arg0) {
        return super.equals(arg0);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 59 * hash + Objects.hashCode(this.hora);
        hash = 59 * hash + Objects.hashCode(this.minuto);
        hash = 59 * hash + Objects.hashCode(this.pmam);
        return hash;
    }

}