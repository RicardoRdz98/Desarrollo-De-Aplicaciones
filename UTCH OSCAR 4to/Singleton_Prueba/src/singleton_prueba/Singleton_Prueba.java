/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package singleton_prueba;

/**
 *
 * @author rodri
 */
public class Singleton_Prueba {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        NewSingleton c = NewSingleton.getConfigurador("miurl", "mibaseDatos");

        System.out.println(c.getUrl());

        System.out.println(c.getBaseDatos());
    }

}
