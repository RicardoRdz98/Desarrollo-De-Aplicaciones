
package app;


public class ColaDatos {

    
    String elementos [] = new String[10];
    int h, q;
    
    public void encolar(String e){
        
        if(!estaLlena()){
        
            elementos[q++] = e;
            if(q == elementos.length)
                q = 0;
        }
    }
    
    public String procesar(){
        
        String tmp = null;
        
        if(!estaVacia()){
        
            tmp = elementos[h++];

            if(h == elementos.length)
                h = 0;
            
        }
        
        return tmp;
    }
    
    public boolean estaLlena(){
        return h == q && elementos[q] != null;
    }
    
    public boolean estaVacia(){
        return h == q && elementos[q] == null;
    }

}