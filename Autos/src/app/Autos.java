package app;

import dao.CarroDaoImple;
import java.util.ArrayList;


public class Autos {

    public static void main(String[] args) {
        CarroDaoImple dao = new CarroDaoImple();
        MarcaCarro dao2 = new MarcaCarro();
        
        // AGREGAR
        Carro auto1 = new Carro(0 ,"A5", "2013", "Audi");
        dao.add(auto1);
        
        // BORRAR
        dao.delete(5);
        
        // UM AUTO
        Carro tmp1 = dao.getCarro(1);
        if(tmp1 != null)
            System.out.println("Nombre: " + tmp1.nombre + " Modelo: " + tmp1.modelo + " Marca: " + tmp1.marca);
        
        // TODOS LOS AUTOS
        ArrayList<Carro> lista = dao.getCarros();
        for (Carro carro : lista) {
            System.out.println("Nombre: " + carro.nombre + " Modelo: " + carro.modelo + " Marca: " + carro.marca);
        }
        
        // ACTUALIZAR
        Carro auto2 = new Carro(1 ,"RAM", "2013", "Chevrolet");
        dao.upDate(auto2);
        
        // 1:M
        ArrayList<Carro> tmp2 = dao2.getCarros("Ford");
        for (Carro carro : tmp2) {
            System.out.println("Nombre: " + carro.nombre + " Modelo: " + carro.modelo + " Marca: " + carro.marca);
        }
    }
    
}
