package app;

public class Carro {

    public int id;
    public String nombre;
    public String modelo;
    public String marca;

    public Carro(int idd, String nom, String mod, String mar) {

        id = idd;
        nombre = nom;
        modelo = mod;
        marca = mar;
    }
}
