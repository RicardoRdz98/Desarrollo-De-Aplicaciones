package dao;

import app.Carro;
import java.util.ArrayList;

public interface CarroDao {
    public Carro add(Carro carro);
    
    public void delete(int id);
    
    public void upDate(Carro carro);
    
    public Carro getCarro(int id);
    
    public ArrayList<Carro> getCarros();
}
