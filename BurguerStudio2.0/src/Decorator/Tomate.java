package Decorator;

public class Tomate extends DecoradorHamburguesa{
	private Hamburguesa hamburguesa;

	public Tomate(Hamburguesa h){
		this.hamburguesa = h;
	}

	public String getDescripcion(){
		return hamburguesa.getDescripcion()+" + Tomate";
	}
}