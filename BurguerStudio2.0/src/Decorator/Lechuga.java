package Decorator;

public class Lechuga extends DecoradorHamburguesa{
	private Hamburguesa hamburguesa;

	public Lechuga(Hamburguesa h){
		this.hamburguesa = h;
	}

	public String getDescripcion(){
		return hamburguesa.getDescripcion()+" + lechuga";
	}
}