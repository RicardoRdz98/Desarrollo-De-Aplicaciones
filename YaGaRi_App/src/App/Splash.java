package App;

import java.awt.Color;

public class Splash extends javax.swing.JFrame {

    public Splash() {
        initComponents();
        setLocationRelativeTo(null); //Localizacion Centrada
        iniciarhilo();
        getContentPane().setBackground(Color.black);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        JPB = new javax.swing.JProgressBar();
        JL_Porcentaje1 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        JL_Porcentaje2 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setBackground(new java.awt.Color(0, 0, 0));
        setUndecorated(true);
        setPreferredSize(new java.awt.Dimension(500, 500));
        setSize(new java.awt.Dimension(500, 400));

        JPB.setForeground(new java.awt.Color(59, 194, 58));

        JL_Porcentaje1.setFont(new java.awt.Font("Rockwell", 0, 14)); // NOI18N
        JL_Porcentaje1.setForeground(new java.awt.Color(59, 194, 58));
        JL_Porcentaje1.setText("jLabel2");

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Res/BackGround_Splash.png"))); // NOI18N

        JL_Porcentaje2.setFont(new java.awt.Font("Rockwell", 0, 14)); // NOI18N
        JL_Porcentaje2.setForeground(new java.awt.Color(59, 194, 58));
        JL_Porcentaje2.setText("ADMINISTRATION ");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(JPB, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(JL_Porcentaje1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(JL_Porcentaje2)))
                .addContainerGap())
            .addGroup(layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jLabel1)
                .addGap(5, 5, 5)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(JL_Porcentaje1)
                    .addComponent(JL_Porcentaje2))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(JPB, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void iniciarhilo() {
        Thread hilo = new Thread(new Runnable() {
            int x = 0;
            @Override
            public void run() {
                try {
                    while(x < 101){
                        JPB.setValue(x);
                        JL_Porcentaje1.setText(x+"%");
                        x++;
                        Thread.sleep(100);
                    }
                    dispose();
                    Login login = new Login();
                    login.setResizable(false);
                    login.show();
                    // Aqui va el objeto de la ventana a desplegar
                } catch (Exception e) {
                    System.out.println(e);
                }
            }
        });
        hilo.start();
    }

    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Splash().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel JL_Porcentaje1;
    private javax.swing.JLabel JL_Porcentaje2;
    private javax.swing.JProgressBar JPB;
    private javax.swing.JLabel jLabel1;
    // End of variables declaration//GEN-END:variables

}