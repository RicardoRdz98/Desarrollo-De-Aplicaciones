package modelo;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class ConexionBD {
    
    private Connection cnn;
    private Statement st;
    
    public ConexionBD() throws ClassNotFoundException, SQLException{
        
        Class.forName("com.mysql.jdbc.Driver");
        
        cnn = DriverManager.getConnection(
                "jdbc:mysql://localhost/webapp",
                "root",
                "root"
        );
        
    }
    
    public void ejecutarSQL(String sql) throws SQLException{
        st = cnn.createStatement();
        st.execute(sql);
    }
    
    public ResultSet consultar(String sql) throws SQLException{
        return cnn.createStatement().executeQuery(sql);
    }
}
